import java.util.regex.*;

public class numero10 {
    public static void main(String[] args) {
        String texto = "La casa de papel es una serie de televisión española.";
        Pattern patron = Pattern.compile("casa");

        Matcher matcher = patron.matcher(texto);
        if (matcher.find()) {
            System.out.println("Se encontró el patrón en la posición " + matcher.start());
        } else {
            System.out.println("No se encontró el patrón.");
        }
    }
}
