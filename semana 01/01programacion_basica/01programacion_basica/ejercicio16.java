import java.util.Scanner;

public class ejercicio16 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double peso, altura, imc;

        // Pedir al usuario que ingrese su peso y su altura
        System.out.print("Ingrese su peso en kilogramos: ");
        peso = sc.nextDouble();

        System.out.print("Ingrese su altura en metros: ");
        altura = sc.nextDouble();

        // Calcular el IMC
        imc = peso / (altura * altura);

        // Imprimir el resultado
        System.out.printf("Su índice de masa corporal (IMC) es %.2f", imc);
    }
}
