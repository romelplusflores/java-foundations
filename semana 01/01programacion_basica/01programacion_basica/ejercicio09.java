import java.util.Scanner;

public class ejercicio09 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Solicitar al usuario que ingrese el radio del círculo
        System.out.print("Ingrese el radio del círculo: ");
        double radio = scanner.nextDouble();
        
        // Calcular el área del círculo
        double area = Math.PI * Math.pow(radio, 2);
        
        // Calcular la circunferencia del círculo
        double circunferencia = 2 * Math.PI * radio;
        
        // Imprimir los resultados
        System.out.println("El área del círculo es: " + area);
        System.out.println("La circunferencia del círculo es: " + circunferencia);
        
        scanner.close();
    }
}

