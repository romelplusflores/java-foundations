import java.util.Scanner;

public class ejercicio19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingresa un número entero: ");
        int num = sc.nextInt();

        int suma = 0;

        for(int i = 1; i <= num; i++) {
            suma += i;
        }

        System.out.println("La suma de los números enteros desde 1 hasta " + num + " es " + suma);

        sc.close();
    }
}
