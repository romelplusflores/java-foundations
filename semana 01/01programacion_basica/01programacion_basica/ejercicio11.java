import java.util.Scanner;

public class ejercicio11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Solicitar al usuario que ingrese dos números enteros
        System.out.print("Ingrese el primer número entero: ");
        int numero1 = scanner.nextInt();
        System.out.print("Ingrese el segundo número entero: ");
        int numero2 = scanner.nextInt();
        
        // Determinar el número mayor
        int mayor;
        if (numero1 > numero2) {
            mayor = numero1;
        } else {
            mayor = numero2;
        }
        
        // Imprimir el número mayor
        System.out.println("El número mayor es " + mayor);
        
        scanner.close();
    }
}
