import java.util.Scanner;

public class ejercicio20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingresa el primer número entero: ");
        int num1 = sc.nextInt();

        System.out.print("Ingresa el segundo número entero: ");
        int num2 = sc.nextInt();

        int mcd = calcularMCD(num1, num2);
        int mcm = calcularMCM(num1, num2, mcd);

        System.out.println("El MCD de " + num1 + " y " + num2 + " es " + mcd);
        System.out.println("El MCM de " + num1 + " y " + num2 + " es " + mcm);

        sc.close();
    }

    public static int calcularMCD(int a, int b) {
        if(b == 0) {
            return a;
        } else {
            return calcularMCD(b, a % b);
        }
    }

    public static int calcularMCM(int a, int b, int mcd) {
        return (a * b) / mcd;
    }
}
