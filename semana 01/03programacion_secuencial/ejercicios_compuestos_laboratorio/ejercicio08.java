import java.util.Scanner;

public class ejercicio08 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("el precio sin descuento: ");
        double precioInicial = sc.nextDouble();
        
        double descuento = 0.14;
        double precioFinal = precioInicial * (1 - descuento);
        
        System.out.println("El precio final con descuento es: " + precioFinal);
    }
}

 el precio sin descuento: 16
El precio final con descuento es: 13.76