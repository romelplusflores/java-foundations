import java.util.Scanner;

public class ejercicio07 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Ingrese el radio del cilindro: ");
        double radio = input.nextDouble();
        
        System.out.print("Ingrese la altura del cilindro: ");
        double altura = input.nextDouble();
        
        double superficieLateral = 2 * Math.PI * radio * altura;
        double volumen = Math.PI * Math.pow(radio, 2) * altura;
        
        System.out.println("El área de la superficie lateral del cilindro es: " + superficieLateral);
        System.out.println("El volumen del cilindro es: " + volumen);
    }
}

Ingrese el radio del cilindro: 5
Ingrese la altura del cilindro: 8
El área de la superficie lateral del cilindro es: 251.32741228718345
El volumen del cilindro es: 628.3185307179587