import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print(" la velocidad en Km/h: ");
    double velocidadKmh = scanner.nextDouble();

    double velocidadMs = velocidadKmh / 3.6;

    System.out.println(velocidadKmh + " Km/h equivale a " + velocidadMs + " m/s.");

    scanner.close();
  }
}
