import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print(" un número entero de 5 cifras: ");
    int numero = scanner.nextInt();

    int digito1 = numero / 10000;
    int digito2 = (numero / 1000) % 10;
    int digito3 = (numero / 100) % 10;
    int digito4 = (numero / 10) % 10;
    int digito5 = numero % 10;

    System.out.print("Las cifras desde el principio son: ");
    System.out.print(digito1 + ", ");
    System.out.print(digito1 * 10 + digito2 + ", ");
    System.out.print(digito1 * 100 + digito2 * 10 + digito3 + ", ");
    System.out.print(digito1 * 1000 + digito2 * 100 + digito3 * 10 + digito4 + ", ");
    System.out.println(digito1 * 10000 + digito2 * 1000 + digito3 * 100 + digito4 * 10 + digito5);

    scanner.close();
  }
}
