import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print(" la cantidad de grados Celsius: ");
    double celsius = scanner.nextDouble();

    double fahrenheit = 32 + (9 * celsius / 5);

    System.out.println(celsius + " grados Celsius equivalen a " + fahrenheit + " grados Fahrenheit.");

    scanner.close();
  }
}
