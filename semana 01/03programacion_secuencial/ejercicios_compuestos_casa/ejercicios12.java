import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print(" un número entero de 5 cifras: ");
    int numero = scanner.nextInt();

    int digito5 = numero % 10;
    int digito4 = (numero / 10) % 10;
    int digito3 = (numero / 100) % 10;
    int digito2 = (numero / 1000) % 10;
    int digito1 = numero / 10000;

    System.out.print("Las cifras desde el final son: ");
    System.out.print(digito5 + ", ");
    System.out.print(digito4 + ", ");
    System.out.print(digito3 + ", ");
    System.out.print(digito2 + ", ");
    System.out.println(digito1);

    scanner.close();
  }
}
