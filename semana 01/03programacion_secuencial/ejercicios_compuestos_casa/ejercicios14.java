import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print(" el precio de compra: ");
    double precioCompra = scanner.nextDouble();
    System.out.print(" el porcentaje de beneficio deseado (por ejemplo, 20 para un 20%): ");
    double porcentajeBeneficio = scanner.nextDouble();

    double precioFinal = precioCompra * (1 + porcentajeBeneficio/100);

    System.out.println("El precio final de venta es: " + precioFinal);

    scanner.close();
  }
}
