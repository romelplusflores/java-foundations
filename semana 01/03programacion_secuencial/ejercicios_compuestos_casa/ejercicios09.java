import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print(" la longitud del primer lado del triángulo: ");
    double lado1 = scanner.nextDouble();
    System.out.print(" la longitud del segundo lado del triángulo: ");
    double lado2 = scanner.nextDouble();
    System.out.print(" la longitud del tercer lado del triángulo: ");
    double lado3 = scanner.nextDouble();

    double s = (lado1 + lado2 + lado3) / 2.0;
    double area = Math.sqrt(s * (s - lado1) * (s - lado2) * (s - lado3));

    System.out.println("El área del triángulo es: " + area);

    scanner.close();
  }
}
