import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int contador = 0;
        do {
            System.out.print("Introduce un número (-1 para terminar): ");
            int numero = scanner.nextInt();

            if (numero != -1 && numero % 10 == 2) {
                contador++;
            }
        } while (numero != -1);

        System.out.println("Has introducido " + contador + " números que acaban en 2.");

        scanner.close();
    }
}
