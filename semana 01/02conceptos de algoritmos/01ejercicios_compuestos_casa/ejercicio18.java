public class ejercicio18 {

    public static void main(String[] args) {
        int inicio = 1;
        int fin = 100;

        int contador = 0;

        for (int numero = inicio; numero <= fin; numero++) {
            int sumaDigitos = sumarDigitos(numero);

            if (numero % sumaDigitos == 0) {
                contador++;
            }
        }

        System.out.println("Hay " + contador + " números de Harshad en el rango del " + inicio + " al " + fin);
    }

    public static int sumarDigitos(int numero) {
        int suma = 0;

        while (numero > 0) {
            suma += numero % 10;
            numero /= 10;
        }

        return suma;
    }
}
