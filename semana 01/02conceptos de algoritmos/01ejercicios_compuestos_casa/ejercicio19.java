public class ejercicio19 {

    public static void main(String[] args) {
        String[] palabras = {"Java", "Python", "Csharp", "Ruby", "Perl", "PHP"};

        for (String palabra : palabras) {
            boolean todasLasLetrasDiferentes = true;
            Set<Character> letrasVistas = new HashSet<>();

            for (char letra : palabra.toCharArray()) {
                if (letrasVistas.contains(letra)) {
                    todasLasLetrasDiferentes = false;
                    break;
                }
                letrasVistas.add(letra);
            }

            if (todasLasLetrasDiferentes) {
                System.out.println(palabra + " tiene todas las letras diferentes.");
            } else {
                System.out.println(palabra + " no tiene todas las letras diferentes.");
            }
        }
    }
}
