public class ejercicio14 {

    public static void main(String[] args) {
        int[] numeros = {45, 297, 703, 9, 10, 2973};
        int contador = 0;

        for (int numero : numeros) {
            if (esNumeroDeKaprekar(numero)) {
                contador++;
            }
        }

        System.out.println("Encontrados " + contador + " números de Kaprekar.");
    }

    public static boolean esNumeroDeKaprekar(int numero) {
        int cuadrado = numero * numero;
        String cuadradoString = String.valueOf(cuadrado);

        for (int i = 1; i < cuadradoString.length(); i++) {
            int parte1 = Integer.parseInt(cuadradoString.substring(0, i));
            int parte2 = Integer.parseInt(cuadradoString.substring(i));

            if (parte2 != 0 && parte1 + parte2 == numero) {
                return true;
            }
        }

        return false;
    }
}
