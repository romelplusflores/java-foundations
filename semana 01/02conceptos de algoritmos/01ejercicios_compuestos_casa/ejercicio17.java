public class ejercicio17 {

    public static void main(String[] args) {
        String[] palabras = {"programacion", "java", "palabra", "abbas", "alumno", "arbol"};

        for (String palabra : palabras) {
            boolean encontrada = false;

            for (int i = 0; i < palabra.length(); i++) {
                char letra = palabra.charAt(i);

                if (palabra.indexOf(letra) != palabra.lastIndexOf(letra)) {
                    if (palabra.lastIndexOf(letra) - palabra.indexOf(letra) == 1) {
                        encontrada = true;
                        break;
                    }
                }
            }

            if (encontrada) {
                System.out.println(palabra + " tiene una letra que aparece exactamente dos veces en cualquier posición.");
            }
        }
    }
}
