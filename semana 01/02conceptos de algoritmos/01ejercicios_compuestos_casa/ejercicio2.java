public class ejercicio2 {
    public static void main(String[] args) {
        String texto = "Este es un texto de ejemplo con varias palabras que tienen vocales consecutivas";
        String[] palabras = texto.split("\\s+"); // separar el texto en palabras

        int contador = 0; // contador de palabras con vocales consecutivas
        for (String palabra : palabras) {
            if (palabra.matches(".*[aeiou]{3,}.*")) { // verificar si la palabra tiene más de dos vocales consecutivas
                contador++;
            }
        }

        System.out.println("El texto tiene " + contador + " palabras con más de dos vocales consecutivas.");
    }
}
