public class ejercicio6 {
  public static void main(String[] args) {
    int num = 1;
    boolean divisible = false;
    
    while (!divisible) {
      boolean isDivisible = true;
      
      for (int i = 1; i <= 20; i++) {
        if (num % i != 0) {
          isDivisible = false;
          break;
        }
      }
      
      if (isDivisible) {
        divisible = true;
        System.out.println(num + " es divisible por todos los números del 1 al 20");
      } else {
        num++;
      }
    }
  }
}
