public class ejercicio12 {

    public static void main(String[] args) {
        int inicio = 1; // inicio del rango de números
        int fin = 1000; // fin del rango de números
        int contador = 0; // contador de números de Smith encontrados

        for (int i = inicio; i <= fin; i++) {
            if (esNumeroSmith(i)) {
                contador++;
            }
        }

        System.out.println("Encontrados " + contador + " numeros de Smith en el rango [" + inicio + ", " + fin + "].");
    }

    public static boolean esNumeroSmith(int n) {
        List<Integer> digitos = descomponerEnDigitos(n); // obtener los dígitos del número
        List<Integer> factores = factorizarEnPrimos(n); // obtener los factores primos del número
        int sumaDigitos = sumarLista(digitos); // sumar los dígitos del número

        // sumar los dígitos de los factores primos
        int sumaFactores = 0;
        for (int factor : factores) {
            sumaFactores += sumarLista(descomponerEnDigitos(factor));
        }

        
        return sumaFactores == sumaDigitos && factores.size() > 1;
    }

   
    public static List<Integer> descomponerEnDigitos(int n) {
        List<Integer> digitos = new ArrayList<>();
        while (n > 0) {
            digitos.add(n % 10);
            n /= 10;
        }
        Collections.reverse(digitos); // invertir la lista para que los dígitos queden en orden correcto
        return digitos;
    }

    
    public static List<Integer> factorizarEnPrimos(int n) {
        List<Integer> factores = new ArrayList<>();
        int factor = 2;
        while (n > 1) {
            if (n % factor == 0) {
                factores.add(factor);
                n /= factor;
            } else {
                factor++;
            }
        }
        return factores;
    }

    
    public static int sumarLista(List<Integer> lista) {
        int suma = 0;
        for (int elemento : lista) {
            suma += elemento;
        }
        return suma;
    }
}
