public class ejercicio13 {

    public static void main(String[] args) {
        List<String> palabras = Arrays.asList("elefante", "banana", "paralelepípedo", "ordenador", "automóvil");
        char letra = 'a';
        int min = 3;
        int max = 4;
        int contador = 0;

        for (String palabra : palabras) {
            if (tieneLetraRepetidaEntreVeces(palabra, letra, min, max)) {
                contador++;
            }
        }

        System.out.println("Encontradas " + contador + " palabras con la letra '" + letra + "' que aparece entre " + min + " y " + max + " veces.");
    }

    public static boolean tieneLetraRepetidaEntreVeces(String palabra, char letra, int min, int max) {
        int contador = 0;
        for (int i = 0; i < palabra.length(); i++) {
            if (palabra.charAt(i) == letra) {
                contador++;
            }
        }
        return contador > min && contador < max;
    }
}
