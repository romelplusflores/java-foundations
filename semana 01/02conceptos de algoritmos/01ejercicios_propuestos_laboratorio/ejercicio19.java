import java.util.Scanner;

public class ejercicio19 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Ingrese un número entero positivo: ");
        int n = scanner.nextInt();
        
        double sumaParcial = 0;
        for (int i = 1; i <= n; i++) {
            sumaParcial += 1.0 / i;
        }
        
        if (Math.round(sumaParcial) == Math.floor(sumaParcial)) {
            System.out.println(n + " es un número armónico.");
        } else {
            System.out.println(n + " no es un número armónico.");
        }
    }

}
