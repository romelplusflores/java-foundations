import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Ingrese una lista de palabras separadas por espacios:");
        String input = scanner.nextLine();
        
        String[] palabras = input.split(" ");
        int contadorPalindromos = 0;
        for (String palabra : palabras) {
            if (esPalindromo(palabra)) {
                contadorPalindromos++;
            }
        }
        
        System.out.println("La cantidad de palabras palíndromos es: " + contadorPalindromos);
    }
    
    private static boolean esPalindromo(String palabra) {
        String reverso = new StringBuilder(palabra).reverse().toString();
        return palabra.equals(reverso);
    }
}
