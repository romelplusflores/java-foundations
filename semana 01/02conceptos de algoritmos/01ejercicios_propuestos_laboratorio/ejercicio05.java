import java.util.Scanner;

public class ejercicio05 {
   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      System.out.print("Ingrese la edad de la persona: ");
      int edad = scanner.nextInt();

      if (edad >= 18) {
         System.out.println("La persona es mayor de edad");
      } else {
         System.out.println("La persona no es mayor de edad");
      }
   }
}
