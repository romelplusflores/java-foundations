import java.util.Scanner;

public class ejercicio18 {

    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Ingrese una lista de palabras separadas por espacios: ");
        String listaPalabras = scanner.nextLine();
        
        System.out.print("Ingrese el número máximo de caracteres que deben tener los factores: ");
        int longitudMaxima = scanner.nextInt();
        
        String[] palabras = listaPalabras.split(" ");
        
        System.out.println("Los siguientes factores tienen una longitud menor que " + longitudMaxima + ":");
        for (String palabra : palabras) {
            for (int i = 1; i <= palabra.length() && i <= longitudMaxima; i++) {
                String factor = palabra.substring(0, i);
                if (factor.length() < longitudMaxima) {
                    System.out.println(factor);
                }
            }
        }
    }

}
