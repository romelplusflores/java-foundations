import java.util.Scanner;

public class ejercicio11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Ingrese una lista de números enteros separados por espacios:");
        String input = scanner.nextLine();
        
        String[] numerosStr = input.split(" ");
        int contadorMultiplosDeTres = 0;
        for (String numeroStr : numerosStr) {
            int numero = Integer.parseInt(numeroStr);
            if (numero % 3 == 0) {
                contadorMultiplosDeTres++;
            }
        }
        
        System.out.println("La cantidad de números múltiplos de tres es: " + contadorMultiplosDeTres);
    }
}
