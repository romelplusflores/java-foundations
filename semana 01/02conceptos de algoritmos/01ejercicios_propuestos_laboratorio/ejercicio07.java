import java.util.Scanner;

public class ejercicio07 {
   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      System.out.print("Ingrese una cadena de caracteres: ");
      String cadena = scanner.nextLine();

      // Eliminamos los espacios en blanco y convertimos todo a minúsculas para evitar problemas de comparación.
      cadena = cadena.replaceAll("\\s", "").toLowerCase();

      int longitud = cadena.length();
      boolean esPalindromo = true;

      for (int i = 0; i < longitud / 2; i++) {
         if (cadena.charAt(i) != cadena.charAt(longitud - 1 - i)) {
            esPalindromo = false;
            break;
         }
      }

      if (esPalindromo) {
         System.out.println("La cadena es un palíndromo");
      } else {
         System.out.println("La cadena no es un palíndromo");
      }
   }
}
