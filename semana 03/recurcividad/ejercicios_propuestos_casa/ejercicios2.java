public class ejercicios2 {
    public static void main(String[] args) {
        int[] array = {5, 10, 2, 8, 3};
        int maximo = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] > maximo) {
                maximo = array[i];
            }
        }
        System.out.println("El máximo valor en el array es: " + maximo);
    }
}

El máximo valor en el array es: 10
