import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ejercicio8 {

    public static void main(String[] args) {
        
        File archivoLectura = new File("archivo.txt");
        Scanner lector;
        try {
            lector = new Scanner(archivoLectura);
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
            return;
        }
        
        while (lector.hasNextLine()) {
            String linea = lector.nextLine();
            System.out.println(linea);
        }
        lector.close();
        
        File archivoEscritura = new File("archivo.txt");
        PrintWriter escritor;
        try {
            escritor = new PrintWriter(archivoEscritura);
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
            return;
        }
        
        escritor.println("¡Los de SENATI son unos tigres en programación!");
        escritor.println("Ejemplo de escritura de un archivo de texto.");
        escritor.close();
        
        System.out.println("Archivo actualizado.");
    }

}