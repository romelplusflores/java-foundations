import java.util.Random;

public class ejercicio7 {

    public static void main(String[] args) {
        int[] datos = generarDatos(10);
        System.out.println("Datos sin ordenar:");
        imprimirDatos(datos);
        quickSort(datos, 0, datos.length - 1);
        System.out.println("Datos ordenados:");
        imprimirDatos(datos);
    }

    public static int[] generarDatos(int n) {
        int[] datos = new int[n];
        Random rand = new Random();
        for (int i = 0; i < n; i++) {
            datos[i] = rand.nextInt(300);
        }
        return datos;
    }

    public static void quickSort(int[] datos, int izquierda, int derecha) {
        if (izquierda < derecha) {
            int indicePivote = particion(datos, izquierda, derecha);
            quickSort(datos, izquierda, indicePivote - 1);
            quickSort(datos, indicePivote + 1, derecha);
        }
    }

    public static int particion(int[] datos, int izquierda, int derecha) {
        int pivote = datos[derecha];
        int indiceMenor = izquierda - 1;
        for (int i = izquierda; i < derecha; i++) {
            if (datos[i] < pivote) {
                indiceMenor++;
                int temp = datos[i];
                datos[i] = datos[indiceMenor];
                datos[indiceMenor] = temp;
            }
        }
        int temp = datos[indiceMenor + 1];
        datos[indiceMenor + 1] = datos[derecha];
        datos[derecha] = temp;
        return indiceMenor + 1;
    }

    public static void imprimirDatos(int[] datos) {
        for (int dato : datos) {
            System.out.print(dato + " ");
        }
        System.out.println();
    }
}




Datos sin ordenar:
10 273 54 109 49 193 34 117 152 218 
Datos ordenados:
10 34 49 54 109 117 152 193 218 273 
