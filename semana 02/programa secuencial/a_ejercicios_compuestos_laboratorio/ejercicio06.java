///Crear un método que busque un elemento en un arreglo de enteros y devuelva su posición.///

class ejercicio06 {
    public static void main(String[] args) {

        int[] numeros = { 50, 21, 6, 97, 18 };
        int numeroBuscado = 20;
        int posicionDeElementoBuscado = existeEnArreglo(numeros, numeroBuscado);
        if (posicionDeElementoBuscado == -1) {
            System.out.println("El elemento NO existe en el arreglo");
        } else {
            System.out.println("El elemento existe en la posición: " + posicionDeElementoBuscado);
        }

        String[] canciones = { "Amor Moderno", "Sentir", "Espiritu en el cielo" };
        String cancionBuscada = "Spirit in the sky";
        posicionDeElementoBuscado = existeEnArreglo(canciones, cancionBuscada);
        if (posicionDeElementoBuscado == -1) {
            System.out.println("El elemento NO existe en el arreglo");
        } else {
            System.out.println("El elemento existe en la posición: " + posicionDeElementoBuscado);
        }
        
        float[] medidas = { 1.5f, 25.65f, 55.62f, 78.2f, 502.551f };
        float medidaBuscada = 70.6f;
        posicionDeElementoBuscado = existeEnArreglo(medidas, medidaBuscada);
        if (posicionDeElementoBuscado == -1) {
            System.out.println("El elemento NO existe en el arreglo");
        } else {
            System.out.println("El elemento existe en la posición: " + posicionDeElementoBuscado);
        }
    }

    public static int existeEnArreglo(int[] arreglo, int busqueda) {
        for (int x = 0; x < arreglo.length; x++) {
            if (arreglo[x] == busqueda) {
                return x;
            }
        }
        return -1;
    }

    public static int existeEnArreglo(String[] arreglo, String busqueda) {
        for (int x = 0; x < arreglo.length; x++) {
            if (arreglo[x].equals(busqueda)) {
                return x;
            }
        }
        return -1;
    }

    public static int existeEnArreglo(float[] arreglo, float busqueda) {
        for (int x = 0; x < arreglo.length; x++) {
            if (arreglo[x] == busqueda) {
                return x;
            }
        }
        return -1;
    }
}
