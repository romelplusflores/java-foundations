public class ejercicios11 {
    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4, 5};
        double average = findAverage(nums);

        System.out.println("La media del arreglo es: " + average);
    }

    private static double findAverage(int[] nums) {
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        return (double) sum / nums.length;
    }
}

La media del arreglo es: 3.0