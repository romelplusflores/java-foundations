import java.util.Arrays;

public class ejercicios05 {
    public static void main(String[] args) {
        int[] arr = {3, 10, 2, 1, 20};
        int n = arr.length;
        int[] lis = new int[n];
        Arrays.fill(lis, 1);
        int maxLength = 1;
        int maxIndex = 0;

        for (int i = 1; i < n; i++) {
            for (int j = 0; j < i; j++) {
                if (arr[i] > arr[j] && lis[i] < lis[j] + 1) {
                    lis[i] = lis[j] + 1;
                    if (maxLength < lis[i]) {
                        maxLength = lis[i];
                        maxIndex = i;
                    }
                }
            }
        }

        int[] subarray = new int[maxLength];
        subarray[maxLength - 1] = arr[maxIndex];
        maxLength--;
        for (int i = maxIndex - 1; i >= 0; i--) {
            if (arr[i] < arr[maxIndex] && lis[i] == maxLength) {
                subarray[maxLength - 1] = arr[i];
                maxLength--;
                maxIndex = i;
            }
        }

        System.out.println("Subarreglo de longitud máxima en orden creciente:");
        System.out.println(Arrays.toString(subarray));
    }
}

Subarreglo de longitud máxima en orden creciente:
[3, 10, 20]