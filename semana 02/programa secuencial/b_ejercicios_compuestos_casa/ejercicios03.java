public class ejercicios03 {
    public static void main(String[] args) {
        int[][] matrix1 = new int[5][5];
        int[][] matrix2 = new int[3][3];

        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1[i].length; j++) {
                matrix1[i][j] = i * matrix1.length + j + 1;
            }
        }

        for (int i = 0; i < matrix2.length; i++) {
            for (int j = 0; j < matrix2[i].length; j++) {
                matrix2[i][j] = i * matrix2.length + j + 1;
            }
        }

        System.out.println("Matriz 1:");
        printMatrix(matrix1);
        System.out.println("Matriz 2:");
        printMatrix(matrix2);

        int[][] resultMatrix = multiplyMatrix(matrix1, matrix2);

        System.out.println("Matriz resultante:");
        printMatrix(resultMatrix);
    }

    public static int[][] multiplyMatrix(int[][] matrix1, int[][] matrix2) {
        int n = matrix1.length;
        int m = matrix2[0].length;
        int[][] resultMatrix = new int[n][m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                int sum = 0;
                for (int k = 0; k < matrix2.length; k++) {
                    sum += matrix1[i][k] * matrix2[k][j];
                }
                resultMatrix[i][j] = sum;
            }
        }

        return resultMatrix;
    }

    public static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }
}

Matriz 1:
1       2       3       4       5 
6       7       8       9       10
11      12      13      14      15
16      17      18      19      20
21      22      23      24      25
Matriz 2:
1       2       3
4       5       6
7       8       9
Matriz resultante:
30      36      42
90      111     132
150     186     222
210     261     312
270     336     402