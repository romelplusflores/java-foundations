public class ejercicios13 {
    public static void main(String[] args) {
        int[] nums = {5, 3, 10, 2, 8};
        int count = countOdd(nums);

        System.out.println("El arreglo tiene " + count + " números impares.");
    }

    private static int countOdd(int[] nums) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] % 2 != 0) {
                count++;
            }
        }
        return count;
    }
}

El arreglo tiene 2 números impares.