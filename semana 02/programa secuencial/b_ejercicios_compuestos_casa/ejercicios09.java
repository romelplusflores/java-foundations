import java.util.Arrays;

public class ejercicios09 {
    public static void main(String[] args) {
        int[] nums = {2, 3, 5, 7, 11, 13, 17};
        double median = findMedian(nums);

        System.out.println("La mediana del arreglo es: " + median);
    }

    private static double findMedian(int[] nums) {
        Arrays.sort(nums);

        if (nums.length % 2 != 0) {
            return (double) nums[nums.length / 2];
        } else {
            int middleIndex1 = nums.length / 2 - 1;
            int middleIndex2 = nums.length / 2;
            return (double) (nums[middleIndex1] + nums[middleIndex2]) / 2.0;
        }
    }
}

La mediana del arreglo es: 7.0