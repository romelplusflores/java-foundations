public class ejercicios15 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        int count = countEvenNumbers(arr);

        System.out.println("El arreglo contiene " + count + " números pares.");
    }

    private static int countEvenNumbers(int[] arr) {
        int count = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                count++;
            }
        }

        return count;
    }
}

El arreglo contiene 5 números pares.