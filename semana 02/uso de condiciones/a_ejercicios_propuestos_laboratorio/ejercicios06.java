import java.util.Scanner;

public class ejercicios06 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingresa la cantidad de numeros: ");
        int n = sc.nextInt();

        int[] numeros = new int[n];

        for (int i = 0; i < n; i++) {
            System.out.print("Ingresa el numero " + (i + 1) + ": ");
            numeros[i] = sc.nextInt();
        }

        int mcm = numeros[0];

        for (int i = 1; i < n; i++) {
            mcm = calcularMCM(mcm, numeros[i]);
        }

        System.out.println("El Mínimo Común Múltiplo es: " + mcm);
    }

    public static int calcularMCM(int a, int b) {
        return (a * b) / calcularMCD(a, b);
    }

    public static int calcularMCD(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return calcularMCD(b, a % b);
        }
    }
}

Ingresa la cantidad de numeros: 3
Ingresa el numero 1: 5
Ingresa el numero 2: 8
Ingresa el numero 3: 15
El Mínimo Común Múltiplo es: 120
