import java.util.Scanner;

public class ejercicios01 {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número entero N: ");
        int N = sc.nextInt();
        System.out.print("Los números primos entre 2 y " + N + " son: ");
        
        for (int i = 2; i <= N; i++) {
            boolean esPrimo = true;
            for (int j = 2; j <= Math.sqrt(i); j++) {
                if (i % j == 0) {
                    esPrimo = false;
                    break;
                }
            }
            if (esPrimo) {
                System.out.print(i + " ");
            }
        }
    }
}

Ingrese un número entero N: 20
Los números primos entre 2 y 20 son: 2 3 5 7 11 13 17 19 