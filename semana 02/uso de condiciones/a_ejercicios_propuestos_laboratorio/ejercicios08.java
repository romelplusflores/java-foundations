import java.util.Scanner;

public class ejercicios08 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese el tamaño del lado del cuadrado: ");
        int n = sc.nextInt();

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (i % 2 == 0) {
                    System.out.print(" *");
                } else {
                    System.out.print(" *");
                }
            }
            System.out.println();
        }
    }
}

Ingrese el tamaño del lado del cuadrado: 7
 * * * * * * *
 * * * * * * *
 * * * * * * *
 * * * * * * *
 * * * * * * *
 * * * * * * *
 * * * * * * *