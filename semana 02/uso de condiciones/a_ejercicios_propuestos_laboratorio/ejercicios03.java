import java.util.Scanner;

public class ejercicios03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingresa los numeros impares ");
        int n = sc.nextInt();

        int suma = 0;
        int num = 1;

        for (int i = 1; i <= n; i++) {
            suma += num;
            num += 2;
        }

        System.out.println("La suma de los " + n + " primeros numeros impares es: " + suma);
    }
}


Ingresa los  numeros impares: 3
La suma de los 3 primeros numeros impares es: 9