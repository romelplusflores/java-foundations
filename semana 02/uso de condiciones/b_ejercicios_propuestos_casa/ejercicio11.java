import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

public class ejercicio11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("es", "PE"));

        System.out.print("Ingrese el precio total del producto: ");
        double precioTotal = sc.nextDouble();
        System.out.print("Ingrese el número de meses de pago: ");
        int numMeses = sc.nextInt();

        double cuotaMensual = precioTotal / ((Math.pow(2, numMeses) - 1) / (2 - 1));
        double totalPago = cuotaMensual * numMeses;

        System.out.println("El pago mensual es: " + nf.format(cuotaMensual));
        System.out.println("El total de lo que pagará después de los " + numMeses + " meses es: " + nf.format(totalPago));
    }
}
