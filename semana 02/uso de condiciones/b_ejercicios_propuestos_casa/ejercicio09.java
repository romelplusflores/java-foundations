import java.util.Scanner;

public class ejercicio09 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese el número de términos de la serie: ");
        int n = scanner.nextInt();

        System.out.print("Ingrese el primer término de la serie: ");
        double a = scanner.nextDouble();

        System.out.print("Ingrese la razón común de la serie: ");
        double r = scanner.nextDouble();

        double sum = a * (1 - Math.pow(r, n)) / (1 - r);

        System.out.println("La suma de los primeros " + n + " términos de la serie es: " + sum);
    }
}
