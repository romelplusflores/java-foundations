import java.util.Scanner;

public class ejercicio18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int limiteInferior, limiteSuperior, numero, suma = 0, fueraIntervalo = 0;
        boolean hayLimiteInferior = false, hayLimiteSuperior = false;

        do {
            System.out.print("Introduce el límite inferior: ");
            limiteInferior = sc.nextInt();
            if (limiteInferior >= 0) {
                hayLimiteInferior = true;
            } else {
                System.out.println("Error: el límite inferior debe ser un número positivo.");
            }
        } while (!hayLimiteInferior);

        do {
            System.out.print("Introduce el límite superior: ");
            limiteSuperior = sc.nextInt();
            if (limiteSuperior >= limiteInferior) {
                hayLimiteSuperior = true;
            } else {
                System.out.println("Error: el límite superior debe ser mayor o igual que el límite inferior.");
            }
        } while (!hayLimiteSuperior);

        do {
            System.out.print("Introduce un número (0 para terminar): ");
            numero = sc.nextInt();
            if (numero != 0) {
                if (numero > limiteInferior && numero < limiteSuperior) {
                    suma += numero;
                } else {
                    fueraIntervalo++;
                }
                if (numero == limiteInferior || numero == limiteSuperior) {
                    System.out.println("Has introducido un número igual a los límites del intervalo.");
                }
            }
        } while (numero != 0);

        System.out.println("La suma de los números dentro del intervalo es: " + suma);
        System.out.println("Hay " + fueraIntervalo + " números fuera del intervalo.");
    }
}
